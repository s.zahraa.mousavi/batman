package test.zahra.batmanworld

import android.app.Application
import android.content.Context

class App : Application() {
    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        var context: Context = App.applicationContext()
    }

    companion object {
        private var instance: App? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }
}