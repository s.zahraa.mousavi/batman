package test.zahra.batmanworld.viewmodel;

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import test.zahra.batmanworld.model.DB
import test.zahra.batmanworld.model.data_model.Movie
import test.zahra.batmanworld.model.data_model.Search
import test.zahra.batmanworld.model.network.RestClient

class MoviesViewModel : ViewModel() {
    private var disposable: CompositeDisposable = CompositeDisposable()
    private var movies: MutableLiveData<List<Movie>> = MutableLiveData<List<Movie>>()
    private var loadError: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    private var loading: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    var TAG: String = this::class.qualifiedName.toString()

    init {
        disposable = CompositeDisposable()

        fetchMovies();
    }

    fun getMovies(): MutableLiveData<List<Movie>> {
        return movies
    }

    fun getLoadError(): MutableLiveData<Boolean> {
        return loadError
    }

    fun getLoading(): MutableLiveData<Boolean> {
        return loading
    }

    private fun fetchMovies() {
        loading.value = true

        disposable.add(RestClient.Companion.getClient()!!.getMovies().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Search>() {

                    override fun onSuccess(search: Search) {
                        loading.value = false
                        movies.value = search.search
                        loadError.value = false
                        Log.d(TAG, "onSuccess: " + search)
                    }

                    @Override
                    override fun onError(e: Throwable) {
                        loadError.value = true
                        loading.value = false
                        Log.e(TAG, "onError: " + e.message)

                        loadFromLocalDB();
                    }
                }));
    }

    private fun loadFromLocalDB() {
        disposable.add(DB.Companion.getInstance().movieDAO().getAllMovies().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Movie>>() {
                    override fun onSuccess(movie: List<Movie>) {
                        movies.value = (movie);
                    }

                    override fun onError(e: Throwable) {
                        Log.e(TAG, "onError: " + e.message)
                    }
                }))

    }
}
