package test.zahra.batmanworld.viewmodel;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import test.zahra.batmanworld.App;
import test.zahra.batmanworld.model.DB;
import test.zahra.batmanworld.model.data_model.MovieDetails;
import test.zahra.batmanworld.model.network.RestClient;

class DetailsViewModel : ViewModel() {
    private var disposable: CompositeDisposable
    private var movieDetailsLiveData: MutableLiveData<MovieDetails> = MutableLiveData<MovieDetails>();
    private var loading: MutableLiveData<Boolean> = MutableLiveData<Boolean>()
    private var error: MutableLiveData<Boolean> = MutableLiveData<Boolean>();

    init {
        disposable = CompositeDisposable()
    }

    fun getLoading(): MutableLiveData<Boolean> {
        return loading;
    }

    fun getError(): MutableLiveData<Boolean> {
        return error;
    }

    fun getDetails(imdbId: String): LiveData<MovieDetails> {
        RestClient.Companion.getClient()!!.getDetails(imdbId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : DisposableSingleObserver<MovieDetails>() {

                    override fun onSuccess(movieDetails: MovieDetails) {
                        movieDetailsLiveData.value = movieDetails
                        loading.value = false
                        error.value = false
                    }

                    override fun onError(e: Throwable) {
                        loading.value = false
                        error.value = true

                        loadFromLocalDB(imdbId)
                    }
                })

        return movieDetailsLiveData
    }

    private fun loadFromLocalDB(imdbId: String) {
        disposable.add(DB.Companion.getInstance().detailsDAO().getDetails(imdbId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<MovieDetails>() {
                    override fun onSuccess(movieDetails: MovieDetails) {
                        movieDetailsLiveData.value = movieDetails
                    }

                    override fun onError(e: Throwable) {
                        Toast.makeText(App.Companion.applicationContext(), e.message, Toast.LENGTH_SHORT).show();
                    }
                }))

    }
}
