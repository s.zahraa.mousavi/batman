package test.zahra.batmanworld.custom_ui.edittext

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText

class EditTextRegular : AppCompatEditText {
    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        try {
            val typeface = Typeface.createFromAsset(context.assets, "fonts/Product Sans Regular.ttf")
        } catch (e: Exception) {
        }
    }
}