package test.zahra.batmanworld.view.activity;

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie_details.*
import test.zahra.batmanworld.R
import test.zahra.batmanworld.model.data_model.MovieDetails
import test.zahra.batmanworld.viewmodel.DetailsViewModel

class MovieDetailsActivity : BaseActivity() {

    private val viewModel: DetailsViewModel by lazy { ViewModelProviders.of(this).get(DetailsViewModel::class.java) };
    private val TAG: String? = this::class.qualifiedName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        val imdbId: String = intent.getStringExtra("imdbId");
        viewModel.getDetails(imdbId).observe(this, Observer { details ->
            bindViews(details)
        });

        viewModel.getLoading().observe(this, Observer { isLoading ->
            if (isLoading != null) {
                if (isLoading)
                    prgDetails.visibility = VISIBLE
                else
                    prgDetails.visibility = GONE
            }
        });

        viewModel.getError().observe(this, Observer { isError ->
            {
                Toast.makeText(applicationContext, "error occur", Toast.LENGTH_SHORT).show()
            }
        });

        btnBack.setOnClickListener(View.OnClickListener {
            onBackPressed();
        })
    }

    private fun bindViews(movieDetails: MovieDetails) {
        if (movieDetails.poster!!.contains("http"))
            Picasso.get().load(movieDetails.poster).into(imgMovieDetails);
        else {
            val uri: Uri = Uri.parse("android.resource://" + packageName + "/drawable/" + movieDetails.poster);
            Picasso.get().load(uri).into(imgMovieDetails);
        }
        txtDetailsYear.text = movieDetails.year
        txtDetailsTitle.text = movieDetails.title
        txtDetailsActors.text = movieDetails.actors
        txtDetailsCountry.text = movieDetails.country
        txtDetailsDirector.text = movieDetails.director
        txtDetailsLanguage.text = movieDetails.language
        txtDetailsPlot.text = movieDetails.plot
        txtDetailsRelease.text = movieDetails.released
        txtDetailsRunTime.text = movieDetails.runtime
        txtDetailsWriter.text = movieDetails.writer
        txtDetailsImdb.text = movieDetails.imdbRating
        txtDetailsIMD.text = movieDetails.ratings!!.get(0).value
        if (movieDetails.ratings!!.size > 1) {
            txtDetailsRT.text = movieDetails.ratings.get(1).value
        }
        if (movieDetails.ratings!!.size > 2) {
            txtDetailsMeta.text = movieDetails.ratings.get(2).value
        }
        txtDetailsGenre.text = movieDetails.genre
        txtDetailsMetaScore.text = movieDetails.metascore
        txtDetailsAwards.text = movieDetails.awards
        txtDetailsProduction.text = movieDetails.production
    }

    override fun onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.back_enter, R.anim.back_exit);
    }
}
