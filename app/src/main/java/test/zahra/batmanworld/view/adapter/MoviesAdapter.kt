package test.zahra.batmanworld.view.adapter;

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import androidx.annotation.NonNull
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import test.zahra.batmanworld.App
import test.zahra.batmanworld.R
import test.zahra.batmanworld.custom_ui.textview.TextViewRegular
import test.zahra.batmanworld.model.data_model.Movie
import test.zahra.batmanworld.view.activity.MovieDetailsActivity
import test.zahra.batmanworld.viewmodel.MoviesViewModel

class MoviesAdapter(var context: Context, viewModel: MoviesViewModel, lifecycleOwner: LifecycleOwner) : RecyclerView.Adapter<MoviesAdapter.viewHolder>()
        , Filterable {
    var movies: MutableList<Movie> = mutableListOf<Movie>()
    var filteredMovies: MutableList<Movie> = mutableListOf<Movie>()
    var imdbId: String = ""
    var TAG = this::class.qualifiedName
    var searching = false

    init {
        viewModel.getMovies().observe(lifecycleOwner, Observer { movies1 ->
            if (movies1 != null)
                movies.addAll(movies1)
        })
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        bindViews(holder, position)
    }

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): viewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.li_movies, parent, false);
        return viewHolder(view);
    }

    private fun bindViews(@NonNull holder: viewHolder, position: Int) {
        if (searching) {
            holder.bind(filteredMovies.get(position))
        } else {
            holder.bind(movies.get(position))
        }
    }


    override fun getItemCount(): Int {
        if (searching)
            return filteredMovies.size
        else
            return movies.size
    }

    @NonNull
    override fun getFilter(): Filter {
        return object : Filter() {

            override fun performFiltering(charSequence: CharSequence): FilterResults {
                if (charSequence.isEmpty()) {
                    searching = false
                    filteredMovies.toMutableList().addAll(movies);
                } else {
                    searching = true
                    filteredMovies = getFilterResult(charSequence.toString().toLowerCase())
                }

                var results: FilterResults = FilterResults();
                results.values = filteredMovies;
                results.count = filteredMovies.size;
                return results;
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredMovies = filterResults.values as MutableList<Movie>
                notifyDataSetChanged();
            }
        };
    }

    private fun getFilterResult(query: String): MutableList<Movie> {
        var filtered: MutableList<Movie> = mutableListOf<Movie>()
        for (movie: Movie in movies) {
            if (movie.title!!.toLowerCase().contains(query)) {
                filtered.add(movie)
            }
        }
        return filtered;
    }


    class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var imgMovie: AppCompatImageView
        private var txtMovie: TextViewRegular
        private var imgSeries: ImageView

        init {
            imgMovie = itemView.findViewById(R.id.imgLiMovies);
            txtMovie = itemView.findViewById(R.id.txtLiMovie);
            imgSeries = itemView.findViewById(R.id.imgLiSeries);
        }

        fun bind(movie: Movie) {
            var intent: Intent = Intent(itemView.context, MovieDetailsActivity::class.java)
            if (movie.title!!.length > 10)
                txtMovie.setText(movie.title!!.substring(0, 10) + "...");
            else
                txtMovie.setText(movie.title);

            if (movie.poster!!.contains("http"))
                Picasso.get().load(movie.poster).into(imgMovie);
            else {
                var uri: Uri = Uri.parse("android.resource://" + App.Companion.applicationContext()!!.packageName + "/drawable/" + movie.poster)
                Picasso.get().load(uri).into(imgMovie);
            }

            if (movie.type != null && movie.type.equals("series"))
                imgSeries.visibility = VISIBLE
            else
                imgSeries.visibility = GONE

            imgMovie.setOnClickListener(View.OnClickListener() {
                var imdbId = movie.imdbID;
                intent.putExtra("imdbId", imdbId)
                itemView.context!!.startActivity(intent)
                var activity = itemView.context as Activity
                activity.overridePendingTransition(R.anim.enter, R.anim.exit);
            });
        }
    }


}
