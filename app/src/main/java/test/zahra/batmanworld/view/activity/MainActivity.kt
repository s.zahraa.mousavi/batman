package test.zahra.batmanworld.view.activity;

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import test.zahra.batmanworld.R
import test.zahra.batmanworld.view.adapter.MoviesAdapter
import test.zahra.batmanworld.viewmodel.MoviesViewModel

class MainActivity : BaseActivity() {

    private val viewModel: MoviesViewModel by lazy { ViewModelProviders.of(this).get(MoviesViewModel::class.java) }
    var moviesAdapter: MoviesAdapter? = null
    val TAG: String? = this::class.qualifiedName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecycler();

        viewModel.getMovies().observe(this, Observer { movies ->
            if (movies != null) {
                rclMovies.visibility = View.VISIBLE
            }
        })

        viewModel.getLoadError().observe(this, Observer { isError ->
            if (isError)
                Toast.makeText(this, getResources().getString(R.string.strConnectionError), Toast.LENGTH_SHORT).show();
        })

        viewModel.getLoading().observe(this, Observer { isLoading ->
            if (isLoading != null) {
                if (isLoading)
                    prgMain.visibility = VISIBLE
                else
                    prgMain.visibility = GONE
            }
        })

        edtSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                moviesAdapter!!.getFilter()!!.filter(p0.toString());
            }
        });
    }

    private fun initRecycler() {
        moviesAdapter = MoviesAdapter(this, viewModel, this)
        rclMovies.apply {
            layoutManager = GridLayoutManager(applicationContext, 3)
            adapter = moviesAdapter
            hasFixedSize()
        }
    }
}