package test.zahra.batmanworld.model.data_model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "movie")
data class Movie(

        @ColumnInfo(name = "Title")
        @SerializedName("Title")
        @Expose
        var title: String?,

        @ColumnInfo(name = "Year")
        @SerializedName("Year")
        @Expose
        var year: String?,

        @PrimaryKey
        @ColumnInfo(name = "imdbID")
        @SerializedName("imdbID")
        @Expose
        @NonNull var imdbID: String,

        @ColumnInfo(name = "Type")
        @SerializedName("Type")
        @Expose
        var type: String?,

        @ColumnInfo(name = "Poster")
        @SerializedName("Poster")
        @Expose
        var poster: String?)