package test.zahra.batmanworld.model.data_model;

import androidx.annotation.NonNull
import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import test.zahra.batmanworld.model.RatingConverter
import java.util.List

@Entity(tableName = "movieDetails")
data class MovieDetails(

        @ColumnInfo(name = "Title")
        @SerializedName("Title")
        @Expose
        val title: String?,

        @ColumnInfo(name = "Year")
        @SerializedName("Year")
        @Expose
        val year: String?,

        @ColumnInfo(name = "Rated")
        @SerializedName("Rated")
        @Expose
        val rated: String?,

        @ColumnInfo(name = "Released")
        @SerializedName("Released")
        @Expose
        val released: String?,

        @ColumnInfo(name = "Runtime")
        @SerializedName("Runtime")
        @Expose
        val runtime: String?,

        @ColumnInfo(name = "Genre")
        @SerializedName("Genre")
        @Expose
        val genre: String?,

        @ColumnInfo(name = "Director")
        @SerializedName("Director")
        @Expose
        val director: String?,

        @ColumnInfo(name = "Writer")
        @SerializedName("Writer")
        @Expose
        val writer: String?,

        @ColumnInfo(name = "Actors")
        @SerializedName("Actors")
        @Expose
        val actors: String?,

        @ColumnInfo(name = "Plot")
        @SerializedName("Plot")
        @Expose
        val plot: String?,

        @ColumnInfo(name = "Language")
        @SerializedName("Language")
        @Expose
        val language: String?,

        @ColumnInfo(name = "Country")
        @SerializedName("Country")
        @Expose
        val country: String?,

        @ColumnInfo(name = "Awards")
        @SerializedName("Awards")
        @Expose
        val awards: String?,

        @ColumnInfo(name = "Poster")
        @SerializedName("Poster")
        @Expose
        val poster: String?,

        @ColumnInfo(name = "Ratings")
        @SerializedName("Ratings")
        @Expose
        @TypeConverters(RatingConverter::class)
        val ratings: List<Rating>?,

        @ColumnInfo(name = "Metascore")
        @SerializedName("Metascore")
        @Expose
        val metascore: String?,

        @ColumnInfo(name = "imdbRating")
        @SerializedName("imdbRating")
        @Expose
        val imdbRating: String?,

        @ColumnInfo(name = "imdbVotes")
        @SerializedName("imdbVotes")
        @Expose
        val imdbVotes: String?,

        @PrimaryKey
        @ColumnInfo(name = "imdbID")
        @SerializedName("imdbID")
        @Expose
        @NonNull
        val imdbID: String,

        @ColumnInfo(name = "Type")
        @SerializedName("Type")
        @Expose
        val type: String?,

        @ColumnInfo(name = "totalSeasons")
        @SerializedName("totalSeasons")
        @Expose
        val totalSeasons: String?,

        @ColumnInfo(name = "DVD")
        @SerializedName("DVD")
        @Expose
        val dVD: String?,

        @ColumnInfo(name = "BoxOffice")
        @SerializedName("BoxOffice")
        @Expose
        val boxOffice: String?,

        @ColumnInfo(name = "Production")
        @SerializedName("Production")
        @Expose
        val production: String?,

        @ColumnInfo(name = "Website")
        @SerializedName("Website")
        @Expose
        val website: String?,

        @Ignore
        @SerializedName("Response")
        @Expose
        val response: String?
)