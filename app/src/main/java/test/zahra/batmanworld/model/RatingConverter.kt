package test.zahra.batmanworld.model;

import androidx.room.TypeConverter
import com.google.gson.Gson
import test.zahra.batmanworld.model.data_model.Rating
import java.util.*

class RatingConverter {

    companion object {
        @TypeConverter
        fun jsonToRating(json: String): List<Rating> {
            var ratings = Gson().fromJson(json, Array<Rating>::class.java).asList()
            var ratingList: List<Rating> = Collections.emptyList()

            for (rating: Rating in ratings) {
                ratingList.toMutableList().add(rating);
            }

            return ratingList;
        }

        @TypeConverter
        public fun ratingToJson(ratingList: List<Rating>): String {
            return Gson().toJson(ratingList);
        }
    }
}

