package test.zahra.batmanworld.model;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import test.zahra.batmanworld.App;
import test.zahra.batmanworld.model.dao.DetailsDAO;
import test.zahra.batmanworld.model.dao.MovieDAO;
import test.zahra.batmanworld.model.data_model.Movie;
import test.zahra.batmanworld.model.data_model.MovieDetails;

@Database(entities = arrayOf(Movie::class, MovieDetails::class), version = 3)
@TypeConverters(RatingConverter::class)
abstract class DB : RoomDatabase() {

    abstract fun movieDAO(): MovieDAO;

    abstract fun detailsDAO(): DetailsDAO;

    companion object {
        fun getInstance(): DB {
            var INSTANCE: DB? = null;

            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(App.Companion.applicationContext(), DB::class.java, "batman")
                        .createFromAsset("database/batman.db")
                        .build();
            }
            return INSTANCE;
        }
    }
}


