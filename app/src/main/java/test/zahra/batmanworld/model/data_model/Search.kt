package test.zahra.batmanworld.model.data_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

data class Search(

        @SerializedName("Search")
        @Expose
        var search: List<Movie>?,
        @SerializedName("totalResults")
        @Expose
        var totalResults: String,
        @SerializedName("Response")
        @Expose
        var response: String

)