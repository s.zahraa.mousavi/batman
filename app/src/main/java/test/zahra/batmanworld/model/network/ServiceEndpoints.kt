package test.zahra.batmanworld.model.network;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import test.zahra.batmanworld.model.data_model.MovieDetails;
import test.zahra.batmanworld.model.data_model.Search;

interface ServiceEndpoints {
    @GET("/?apikey=3e974fca&s=batman")
    fun getMovies(): Single<Search>;

    @GET("/?apikey=3e974fca")
    fun getDetails(@Query("i") imdbId: String): Single<MovieDetails>

}
