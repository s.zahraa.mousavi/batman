package test.zahra.batmanworld.model.dao;

import androidx.room.Dao;
import androidx.room.Query;

import io.reactivex.Single;
import test.zahra.batmanworld.model.data_model.Movie;

@Dao
interface MovieDAO {

    @Query("SELECT * FROM movie")
    fun getAllMovies(): Single<List<Movie>>;
}
