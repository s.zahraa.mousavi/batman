package test.zahra.batmanworld.model.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Single;
import test.zahra.batmanworld.model.data_model.Movie;
import test.zahra.batmanworld.model.data_model.MovieDetails;

@Dao
interface DetailsDAO {
    @Query("SELECT * FROM movieDetails WHERE imdbID LIKE :imdbId")
    fun getDetails(vararg imdbId: String): Single<MovieDetails>;
}
